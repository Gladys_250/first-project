 function add(){
            var num1=document.getElementById("num1").value;
            var num2=document.getElementById("num2").value;
            num2=parseFloat(num2);
            num1=parseFloat(num1);

            var result=num2+num1;
            document.getElementById("result").value=result;

        }
		   function multiply(){
            var num3=document.getElementById("num3").value;
            var num4=document.getElementById("num4").value;
            num3=parseFloat(num3);
            num4=parseFloat(num4);

            var result=num3*num4;
            document.getElementById("result").value=result;

        }
        function subtract(){
            var num5=document.getElementById("num5").value;
            var num6=document.getElementById("num6").value;
            num5=parseFloat(num5);
            num6=parseFloat(num6);

            var result=num5-num6;
            document.getElementById("result").value=result;
        }
        function divide(){
            var num7=document.getElementById("num7").value;
            var num8=document.getElementById("num8").value;
            num7=parseFloat(num7);
            num8=parseFloat(num8);

            var result=num7/num8;
            document.getElementById("result").value=result;
        }
        
        

        


     //logical operators
        
        x = 10;
        y = 6;
        if( x==10 && y>6){
            document.getElementById("log").innerHTML = true;

        }
        else
        {
            document.getElementById("log").innerHTML = false;
        }

        var x = 20;
       var y = 10;

     document.getElementById("log").innerHTML = (x == 15 || y == 15) + "<br>" +   (x == 1 || y == 5) + "<br>" +  (x == 0 || y == 10) + "<br>" +  (x == 20 || y == 10);
      

      
    var name = 'John';
    var age = 30;
    

        document.getElementById("log").innerHTML =  !(age == 30) + '<br>' + (name == 'John');
        
      //comparison operators

       x=10;
       if(x==10){
           document.getElementById("try").innerHTML = "it is true"
       }
       x = 6;
       if(x >6){
           document.getElementById("try").innerHTML = "error";//get nothing in the browser because x is not > 6

           
       }
       else if(x == 6){
           document.getElementById("try").innerHTML = "correct";
       }
       else{
           document.getElementById("try").innerHTML = "undefined";
       }

    //functions
    var x = myFunction(10,20);
    document.getElementById("demo").innerHTML = x;
    function myFunction(b,c){//calls a function that performs the calculation and return the result
        return b + c;
    }
    //loops

    var x = 10;
    var number = "";
    
    for(i = 0; i<10; i++){
       number +=i +"</br>";
        
    }
    document.getElementById("demo1").innerHTML = number;

//while loop
    var colors = ["white","blue","yellow","red"];
    var y =0;
    var number = "";
    while(y<colors.length){
        number += colors[y] +"</br>";
        y++;

    }
    document.getElementById("demo2").innerHTML = number;

//event handlers
function press(){
    name = document.forms[0].name.value;
    alert("Hello " + name + "! welcome to the new page");
}
//Arrays



//reversing arrays

var colors = ["red","blue","white","yellow"]; 
document.getElementById("demo3").innerHTML = colors;
function reverse(){
    colors.reverse();
    document.getElementById("demo3").innerHTML = colors;
}
//sorting arrays

var fruits = ["cherries","pineaple","banana","mango"]; 
document.getElementById("demo4").innerHTML = fruits;
function sorting(){
    fruits.sort();
    document.getElementById("demo4").innerHTML = fruits;
}
//unshift() method
var colors = ["black","red","blue"];
document.getElementById("demo9").innerHTML = colors;
function unshifting(){
    colors.unshift("brown","green");
    document.getElementById("demo9").innerHTML = colors
}
//slice() method
function slicing(){
    var fruits = ["banana","orange","pearl","lemon","mango"];
    var items = fruits.slice(1,3);
    document.getElementById("demo10").innerHTML = items;
}
//push()method
var colors = ["black","red","blue"];

document.getElementById("demo11").innerHTML = colors;
function pushing(){
    colors.push("brown","green");
    document.getElementById("demo11").innerHTML = colors
}
//concat() method
var x  = [1,2,3,4,5];
var y = [7,8,9,10];
var z = x.concat(y);
document.getElementById("demo12").innerHTML = z;

//mathematical operators on arrays
//adding 2 array values
var num1 = [1,5,4,3,2];
var num2 = [6,7,8,9,10];

var result = 0;
for(var i =0; i< num1.length; i++){
    result += (num1[i] + num2[i]);
       
}
document.getElementById("demo5").innerHTML = result;
//mulptiply 2 array values
 var arr1 = [1,2,3,4]
 var arr2 = [1,2,3,4]

 var result = 0;
 for(var i=0; i<arr1.length; i++){
     result +=(arr1[i] * arr2[i]);
 }

 document.getElementById("demo6").innerHTML = result;
//dividing 2 array values

var arr1 = [10,20,30,42];
var arr2 = [2,4,5,6];

 var result = 0;
 for(var i=0; i<arr1.length; i++){
     result +=(arr1[i] / arr2[i]);
 }
 document.getElementById("demo7").innerHTML = result;
 //subtract 2 array values

 
var arr1 = [10,9,8,7];
var arr2 = [5,4,3,2];

 var result = 0;
 for(var i=0; i<arr1.length; i++){
     result +=(arr1[i] - arr2[i]);
 }
 document.getElementById("demo8").innerHTML = result;

//regular expressions
let string = "this is Regular expressions are used to match patterns in a string ";
let regex = /regular/i;
let word = string.match(regex);
document.getElementById("demo14").innerHTML = word;

let sample = "this is Regular expressions are used to match patterns in a string ";
let vowelreg = /[aeiuo]/ig;
let give = sample.match(vowelreg);
console.log(give);



let sample2 = "bbbuibbjugh ";
let reg = /[b+]/;
let give1 = sample2.match(reg);
console.log(give1);

let sample3 = "Aaaaaaaahgjro";
let reg1 = /Aa*/;
let give2 = sample3.match(reg1);
console.log(give2);

let text =  "this is Regular expressions are used to match patterns in a string ";
let reg2 = /t[a-z]*?s/g;
let give3 = text.match(reg2);
console.log(give3);


let text1 =  "this is Regular expressions are used to match patterns in a string ";
let reg3 = /^this/;
let give4 = reg3.test(text1);
console.log(give4);
